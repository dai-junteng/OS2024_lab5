#include "asm_utils.h"
#include "interrupt.h"
#include "stdio.h"
#include "program.h"
#include "thread.h"

// 屏幕IO处理器
STDIO stdio;
// 中断管理器
InterruptManager interruptManager;
// 程序管理器
ProgramManager programManager;

void fifth_thread(void *arg) {
    //printf("pid %d name \"%s\": Hello World!\n", programManager.running->pid, programManager.running->name);
    //while (1);
}

void forth_thread(void *arg) {
    //printf("pid %d name \"%s\": Hello World!\n", programManager.running->pid, programManager.running->name);
}

void third_thread(void *arg) {
    //printf("pid %d name \"%s\": Hello World!\n", programManager.running->pid, programManager.running->name);
}
void second_thread(void *arg) {
    //printf("pid %d name \"%s\": Hello World!\n", programManager.running->pid, programManager.running->name);
    //while(1);
}
void first_thread(void *arg)   
{
    // 第1个线程不可以返回
    //printf("pid %d name \"%s\": Hello World!\n", programManager.running->pid, programManager.running->name);
    if (!programManager.running->pid)
    {
        programManager.executeThread(second_thread, nullptr, "second thread", 5);
        programManager.executeThread(third_thread, nullptr,  "third  thread", 4);
        programManager.executeThread(forth_thread, nullptr,  "forth  thread", 2);
        programManager.executeThread(fifth_thread, nullptr, "fifth  thread", 3);
        //while (1);
    }
    asm_halt();
}

extern "C" void setup_kernel()
{

    // 中断管理器
    interruptManager.initialize();
    interruptManager.enableTimeInterrupt();
    interruptManager.setTimeInterrupt((void *)asm_time_interrupt_handler);

    // 输出管理器
    stdio.initialize();

    // 进程/线程管理器
    programManager.initialize();
    
    printf("------This scheduling algorithm is SJF------ \n");
    //printf("stable ticks: 2\n");
    // 创建第一个线程，第一个进程是父进程，其他进程是子进程
    int pid = programManager.executeThread(first_thread, nullptr, "first  thread", 1);
    if (pid==-1)  {
        printf("can not execute thread\n");
        asm_halt();
    }

    ListItem *item = programManager.readyPrograms.front();
    PCB *firstThread = ListItem2PCB(item, tagInGeneralList);
    firstThread->status = RUNNING;
    programManager.readyPrograms.pop_front();
    programManager.running = firstThread;
    asm_switch_thread(0, firstThread);

    asm_halt();
}
