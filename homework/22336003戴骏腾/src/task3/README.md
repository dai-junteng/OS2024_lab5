# 编译
```shell
cd build
make complie
make build
```
或者可以合起来
```shell
cd build
make complie && make build
```

# 运行
使用`run/bochsrc.bxrc`来运行。

#说明
FSFC——先进先出调度
HRRN——最高响应比调度
PS——优先级调度
RR——时间片轮转调度
SJF——短作业优先调度
