#include "program.h"
#include "stdlib.h"
#include "interrupt.h"
#include "asm_utils.h"
#include "stdio.h"
#include "thread.h"
#include "os_modules.h"

const int PCB_SIZE = 4096;                   // PCB的大小，4KB。
char PCB_SET[PCB_SIZE * MAX_PROGRAM_AMOUNT]; // 存放PCB的数组，预留了MAX_PROGRAM_AMOUNT个PCB的大小空间。
bool PCB_SET_STATUS[MAX_PROGRAM_AMOUNT];     // PCB的分配状态，true表示已经分配，false表示未分配。

ProgramManager::ProgramManager()
{
    initialize();
}

void ProgramManager::initialize()
{
    allPrograms.initialize();
    readyPrograms.initialize();
    running = nullptr;

    for (int i = 0; i < MAX_PROGRAM_AMOUNT; ++i)
    {
        PCB_SET_STATUS[i] = false;
    }
}

int ProgramManager::executeThread(ThreadFunction function, void *parameter, const char *name, int priority)
{
    // 关中断，防止创建线程的过程被打断
    bool status = interruptManager.getInterruptStatus();
    interruptManager.disableInterrupt();

    // 分配一页作为PCB
    PCB *thread = allocatePCB();

    if (!thread)
        return -1;

    // 初始化分配的页
    memset(thread, 0, PCB_SIZE);

    for (int i = 0; i < MAX_PROGRAM_NAME && name[i]; ++i)
    {
        thread->name[i] = name[i];
    }

    thread->status = ProgramStatus::READY;
    thread->priority = priority;
    //thread->ticks = priority * 3;
    thread->ticksPassedBy = 0;
    thread->pid = ((int)thread - (int)PCB_SET) / PCB_SIZE;
    //初始化分配时间：ticks表示时间
    if (thread->pid==0) thread->ticks=1;
    if (thread->pid==1) thread->ticks=5;
    if (thread->pid==2) thread->ticks=3;
    if (thread->pid==3) thread->ticks=2;
    if (thread->pid==4) thread->ticks=1;
    
    //printf("###%d %d##",thread->pid,thread->status);
    //输出进程创建
    printf("pid %d name \"%s\": Hello World! thread time:%d priority:%d\n", thread->pid, thread->name,thread->ticks,thread->priority);
    
    // 线程栈
    thread->stack = (int *)((int)thread + PCB_SIZE);
    thread->stack -= 7;
    thread->stack[0] = 0;
    thread->stack[1] = 0;
    thread->stack[2] = 0;
    thread->stack[3] = 0;
    thread->stack[4] = (int)function;
    thread->stack[5] = (int)program_exit;
    thread->stack[6] = (int)parameter;

    allPrograms.push_back(&(thread->tagInAllList));
    
    //优先级排序加入readyPrograms
    //ListItem *cur=&(thread->tagInAllList);
    
    ListItem *item = readyPrograms.front();
    PCB *x = ListItem2PCB(item, tagInGeneralList);
    int flag=1;
    for (int i=1;i<=readyPrograms.size();++i)	{
    	if (x->priority > thread->priority) {  	    
    	    flag=0;
    	    readyPrograms.insert(i-1, &(thread->tagInGeneralList));
    	    /*if (i==1) {
    	        readyPrograms.push_front(&(thread->tagInGeneralList));
	    }
	    else {
	        ListItem *y=item->previous;
	        y->next=cur;   
	        item->previous=cur;
	        cur->previous=y;
	        cur->next=item;
	    }*/
	    break;
    	}
    	item=item->next;
    	x = ListItem2PCB(item, tagInGeneralList);
    }
    if (flag) readyPrograms.push_back(&(thread->tagInGeneralList));
    
    // 恢复中断
    interruptManager.setInterruptStatus(status);

    return thread->pid;
}

void ProgramManager::schedule()
{
    bool status = interruptManager.getInterruptStatus();
    interruptManager.disableInterrupt();  //关闭中断
    if (readyPrograms.size() == 0)
    {
        interruptManager.setInterruptStatus(status);
        return;
    }

  
    if (running->ticks==0) running->status=ProgramStatus::DEAD;
    
    if (running->status == ProgramStatus::RUNNING)
    {
        running->status = ProgramStatus::READY;
        //running->ticks = running->priority * 4;
        readyPrograms.push_back(&(running->tagInGeneralList));
    }
    else if (running->status == ProgramStatus::DEAD)
    {
        releasePCB(running);
    }

    ListItem *item = readyPrograms.front();
    PCB *next = ListItem2PCB(item, tagInGeneralList);
    PCB *cur = running;
    next->status = ProgramStatus::RUNNING;
    running = next;
    readyPrograms.pop_front();
    
    

    //printf("!!!!%d %d %d\n",next->pid,next->ticks,next->status);
    asm_switch_thread(cur, next);  //进程切换

    interruptManager.setInterruptStatus(status);  //恢复中断
}

void program_exit()
{
    PCB *thread = programManager.running;
    //thread->status = ProgramStatus::DEAD;
    if (thread->ticks==0) thread->status = ProgramStatus::DEAD;
    
    if (thread->pid)
    {
    	//while (1);
        //programManager.schedule();
        asm_halt();  //回到中断
    }
    else
    {
    	while (1);
        interruptManager.disableInterrupt();
        printf("halt\n");
        asm_halt();
    }
}

PCB *ProgramManager::allocatePCB()
{
    for (int i = 0; i < MAX_PROGRAM_AMOUNT; ++i)
    {
        if (!PCB_SET_STATUS[i])
        {
            PCB_SET_STATUS[i] = true;
            return (PCB *)((int)PCB_SET + PCB_SIZE * i);
        }
    }

    return nullptr;
}

void ProgramManager::releasePCB(PCB *program)
{
    int index = ((int)program - (int)PCB_SET) / PCB_SIZE;
    PCB_SET_STATUS[index] = false;
}
