# **操作系统原理 实验五**

# 个人信息

【院系】计算机学院

【专业】计算机科学与技术

【学号】22336003

【姓名】戴骏腾

# 实验题目

内核线程

# 实验目的

1. 学习C语言的可变参数机制的实现方法，实现可变参数机制，以及实现一个较为简单的printf函数。
2. 实现内核线程的实现。
3. 重点理解 `asm_switch_thread`是如何实现线程切换的，体会操作系统实现并发执行的原理。
4. 实现基于时钟中断的时间片轮转(RR)调度算法，并实现一个线程调度算法。

# 实验要求

1. 实现了可变参数机制及printf函数。
2. 自行实现PCB，实现线程。
3. 了解线程调度，并实现一个调度算法。
4. 撰写实验报告。

# 实验方案

## assignment1:

### step1：

引入 `<stdarg.h>`头文件实现可变参数的函数：

```
void print_any_number_of_integers(int n, ...);
```

为了引用可变参数列表中的参数，我们需要用到 `<stdarg.h>`头文件定义的一个变量类型 `va_list`和三个宏 `va_start`，`va_arg`，`va_end`，这三个宏用于获取可变参数列表中的参数，用法如下。

| 宏                                      | 用法说明                                                                                                                         |
| --------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------- |
| `va_list`                             | 定义一个指向可变参数列表的指针。                                                                                                 |
| `void va_start(va_list ap, last_arg)` | 初始化可变参数列表指针 `ap`，使其指向可变参数列表的起始位置，即函数的固定参数列表的最后一个参数 `last_arg`的后面第一个参数。 |
| `type va_arg(va_list ap, type)`       | 以类型 `type`返回可变参数，并使 `ap`指向下一个参数。                                                                         |
| `void va_end(va_list ap)`             | 清零 `ap`。                                                                                                                    |

编译运行main.cpp：

```
g++ main.cpp -o main.out && ./main.out
```

这条指令实际上可以分解成两条指令：

```
g++ main.cpp -o main.out 
./main.out
```

里面每个元素的含义：

1. **g++ main.cpp -o main.out**
   * `g++`: 这是GNU的C++编译器。它用于将C++源代码（.cpp文件）编译成可执行文件或库。
   * `main.cpp`: 这是你要编译的C++源代码文件的名称。
   * `-o main.out`: 这是一个选项，告诉 `g++`将编译后的输出（即可执行文件）命名为 `main.out`。默认情况下，`g++`会输出一个名为 `a.out`的可执行文件，但使用 `-o`选项，你可以指定一个不同的输出文件名。
2. **&&**
   * 这是一个shell操作符，它表示“如果前一个命令成功执行（返回值为0），则执行后面的命令”。简单来说，只有当 `g++ main.cpp -o main.out`成功编译了 `main.cpp`并生成了 `main.out`后，才会执行后面的 `./main.out`命令。
3. **./main.out**
   * `./`: 这表示当前目录。在Unix和Linux系统中，`.`代表当前目录。
   * `main.out`: 这是你要执行的可执行文件的名称。这个命令会尝试运行前面由 `g++`编译生成的可执行文件。

### step2：

不引入 `<stdarg.h>`头文件实现可变参数的函数：

自行定义step1中的三个宏定义

```
typedef char *va_list;
#define _INTSIZEOF(n) ((sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1))
#define va_start(ap, v) (ap = (va_list)&v + _INTSIZEOF(v))
#define va_arg(ap, type) (*(type *)((ap += _INTSIZEOF(type)) - _INTSIZEOF(type)))
#define va_end(ap) (ap = (va_list)0)
```

下面解释可以取代的原因：（待补充）

### step3：

在可变参数的基础上实现printf：

fmt解析（原理解释待补充）

实现的printf比较简单，只能解析如下参数。

| 符号 | 含义             |
| ---- | ---------------- |
| %d   | 按十进制整数输出 |
| %c   | 输出一个字符     |
| %s   | 输出一个字符串   |
| %x   | 按16进制输出     |

~~Q1创新点（可以解析多一点东西！！）~~

创新——增加小数部分（这样任务四可以输出时间）

## assignment2：

设计PCB（可以自行添加更多属性，用自己设计的PCB实现线程）

PBC（进程控制块）是描述和切换进程的重要结构

在 `include/thread.h`中定义进程的5种状态同时定义结构体PCB

```
enum ProgramStatus
{
    CREATED,
    RUNNING,
    READY,
    BLOCKED,
    DEAD
};
```

### PCB结构体成员

下面是PCB结构体各成员的含义：

```
struct PCB
{
    int *stack;                      // 栈指针，用于调度时保存esp
    char name[MAX_PROGRAM_NAME + 1]; // 线程名
    enum ProgramStatus status;       // 线程的状态
    int priority;                    // 线程优先级
    int pid;                         // 线程pid
    int ticks;                       // 线程时间片总时间
    int ticksPassedBy;               // 线程已执行时间
    ListItem tagInGeneralList;       // 线程队列标识
    ListItem tagInAllList;           // 线程队列标识
};
```

### ListItem定义

我们下面要定义ListItem，也就是自定义的链表类型：

```
struct ListItem
{
    ListItem *previous;
    ListItem *next;
};
class List
{
public:
    ListItem head;

public:
    // 初始化List
    List();
    // 显式初始化List
    void initialize();
    // 返回List元素个数
    int size();
    // 返回List是否为空
    bool empty();
    // 返回指向List最后一个元素的指针
    // 若没有，则返回nullptr
    ListItem *back();
    // 将一个元素加入到List的结尾
    void push_back(ListItem *itemPtr);
    // 删除List最后一个元素
    void pop_back();
    // 返回指向List第一个元素的指针
    // 若没有，则返回nullptr
    ListItem *front();
    // 将一个元素加入到List的头部
    void push_front(ListItem *itemPtr);
    // 删除List第一个元素
    void pop_front();
    // 将一个元素插入到pos的位置处
    void insert(int pos, ListItem *itemPtr);
    // 删除pos位置处的元素
    void erase(int pos);
    void erase(ListItem *itemPtr);
    // 返回指向pos位置处的元素的指针
    ListItem *at(int pos);
    // 返回给定元素在List中的序号
    int find(ListItem *itemPtr);
};
```

### PCB的分配和释放：

```
//PCB的分配
PCB *ProgramManager::allocatePCB()
{
    for (int i = 0; i < MAX_PROGRAM_AMOUNT; ++i)
    {
        if (!PCB_SET_STATUS[i])
        {
            PCB_SET_STATUS[i] = true;
            return (PCB *)((int)PCB_SET + PCB_SIZE * i);
        }
    }

    return nullptr;
}

//PCB的释放
void ProgramManager::releasePCB(PCB *program)
{
    int index = ((int)program - (int)PCB_SET) / PCB_SIZE;
    PCB_SET_STATUS[index] = false;
}
```

### 线程创建

我们在 `ProgramManager`中声明一个用于创建线程的函数 `executeThread`。

```
class ProgramManager
{
public:
    List allPrograms;   // 所有状态的线程/进程的队列
    List readyPrograms; // 处于ready(就绪态)的线程/进程的队列
    PCB *running;       // 当前执行的线程
public:
    ProgramManager();
    void initialize();

    // 创建一个线程并放入就绪队列
    // function：线程执行的函数
    // parameter：指向函数的参数的指针
    // name：线程的名称
    // priority：线程的优先级
    // 成功，返回pid；失败，返回-1
    int executeThread(ThreadFunction function, void *parameter, const char *name, int priority);

    // 分配一个PCB
    PCB *allocatePCB();
    // 归还一个PCB
    // program：待释放的PCB
    void releasePCB(PCB *program);
};
```

我们在 `src/kernel/program.cpp`中实现 `executeThread`，如下所示。

```
int ProgramManager::executeThread(ThreadFunction function, void *parameter, const char *name, int priority)
{
    // 关中断，防止创建线程的过程被打断
    bool status = interruptManager.getInterruptStatus();
    interruptManager.disableInterrupt();

    // 分配一页作为PCB
    PCB *thread = allocatePCB();

    if (!thread)
        return -1;

    // 初始化分配的页
    memset(thread, 0, PCB_SIZE);

    for (int i = 0; i < MAX_PROGRAM_NAME && name[i]; ++i)
    {
        thread->name[i] = name[i];
    }

    thread->status = ProgramStatus::READY;
    thread->priority = priority;
    thread->ticks = priority * 10;
    thread->ticksPassedBy = 0;
    thread->pid = ((int)thread - (int)PCB_SET) / PCB_SIZE;

    // 线程栈
    thread->stack = (int *)((int)thread + PCB_SIZE);
    thread->stack -= 7;
    thread->stack[0] = 0;
    thread->stack[1] = 0;
    thread->stack[2] = 0;
    thread->stack[3] = 0;
    thread->stack[4] = (int)function;
    thread->stack[5] = (int)program_exit;
    thread->stack[6] = (int)parameter;

    allPrograms.push_back(&(thread->tagInAllList));
    readyPrograms.push_back(&(thread->tagInGeneralList));

    // 恢复中断
    interruptManager.setInterruptStatus(status);

    return thread->pid;
}
```

## assignment3：

创建一个进程并执行（程序有）

创建另一个进程，将两个进程进行切换体现过程

并且要求debug调度

### 进程调度：

```
void ProgramManager::schedule()
{
    bool status = interruptManager.getInterruptStatus();
    interruptManager.disableInterrupt();

    if (readyPrograms.size() == 0)
    {
        interruptManager.setInterruptStatus(status);
        return;
    }

    if (running->status == ProgramStatus::RUNNING)
    {
        running->status = ProgramStatus::READY;
        running->ticks = running->priority * 10;
        readyPrograms.push_back(&(running->tagInGeneralList));
    }
    else if (running->status == ProgramStatus::DEAD)
    {
        releasePCB(running);
    }

    ListItem *item = readyPrograms.front();
    PCB *next = ListItem2PCB(item, tagInGeneralList);
    PCB *cur = running;
    next->status = ProgramStatus::RUNNING;
    running = next;
    readyPrograms.pop_front();

    asm_switch_thread(cur, next);

    interruptManager.setInterruptStatus(status);
}
```

下面来理解上面ProgramManager::schedule的逻辑：

首先，最重要的一点是为了实现进程的互斥，在开始schedule时要关闭中断，退出时再恢复中断，指令如下：

```
bool status = interruptManager.getInterruptStatus();
interruptManager.disableInterrupt();  //关闭中断
interruptManager.setInterruptStatus(status);  //恢复中断
```

第6-9行，我们判断当前可调度的线程数量，如果 `readyProgram`为空，那么说明当前系统中只有一个线程，因此无需进行调度，直接返回即可。

第12-21行，我们判断当前线程的状态，如果是运行态(RUNNING)，则重新初始化其状态为就绪态(READY)和 `ticks`，并放入就绪队列；如果是终止态(DEAD)，则回收线程的PCB。

第23行，我们去就绪队列的第一个线程作为下一个执行的线程。就绪队列的第一个元素是 `ListItem *`类型的，我们需要将其转换为 `PCB`。注意到放入就绪队列 `readyPrograms`的是每一个PCB的 `&tagInGeneralList`，而 `tagInGeneralList`在PCB中的偏移地址是固定的。也就是说，我们将 `item`的值减去 `tagInGeneralList`在PCB中的偏移地址就能够得到PCB的起始地址。我们将上述过程写成一个宏。

```
#define ListItem2PCB(ADDRESS, LIST_ITEM) ((PCB *)((int)(ADDRESS) - (int)&((PCB *)0)->LIST_ITEM))
```

最后，我们就开始将线程从 `cur`切换到 `next`，如下所示，代码放置在 `src/utils/asm_utils.asm`中。线程的所有信息都在线程栈中，只要我们切换线程栈就能够实现线程的切换，线程栈的切换实际上就是将线程的栈指针放到esp中。

```
asm_switch_thread:
    push ebp
    push ebx
    push edi
    push esi

    mov eax, [esp + 5 * 4]
    mov [eax], esp ; 保存当前栈指针到PCB中，以便日后恢复

    mov eax, [esp + 6 * 4]
    mov esp, [eax] ; 此时栈已经从cur栈切换到next栈

    pop esi
    pop edi
    pop ebx
    pop ebp

    sti
    ret
```

第2-5行，我们保存寄存器 `ebp`，`ebx`，`edi`，`esi`。为什么要保存这几个寄存器？这是由C语言的规则决定的，C语言要求被调函数主动为主调函数保存这4个寄存器的值。如果我们不遵循这个规则，那么当我们后面线程切换到C语言编写的代码时就会出错。

第7-8行，我们保存esp的值到线程的 `PCB::statck`中，用做下次恢复。注意到 `PCB::stack`在 `PCB`的偏移地址是0。因此，第7行代码是首先将 `cur->stack`的地址放到 `eax`中，第8行向 `[eax]`中写入 `esp`的值，也就是向 `cur->stack`中写入esp。

**此时，cur指向的PCB的栈结构如下，`PCB::stack`箭头指向的位置就是保存在 `PCB::stack`中的值。**

![1715738028554](image/lab5/1715738028554.png)

第10-11行，我们将 `next->stack`的值写入到esp中，从而完成线程栈的切换。此时，`next`指向的线程有两种状态，一种是刚创建还未调度运行的，一种是之前被换下处理器现在又被调度。这两种状态对应的栈结构有些不一致，对于前者，其结构如下：

![1715738054433](image/lab5/1715738054433.png)

接下来的 `pop`语句会将4个0值放到 `esi`，`edi`，`ebx`，`ebp`中。此时，栈顶的数据是线程需要执行的函数的地址 `function`。执行ret返回后，`function`会被加载进eip，从而使得CPU跳转到这个函数中执行。此时，进入函数后，函数的栈顶是函数的返回地址，返回地址之上是函数的参数，符合函数的调用规则。而函数执行完成时，其执行ret指令后会跳转到返回地址 `program_exit`，如下所示。

```
void program_exit()
{
    PCB *thread = programManager.running;
    thread->status = ThreadStatus::DEAD;

    if (thread->pid)
    {
        programManager.schedule();
    }
    else
    {
        interruptManager.disableInterrupt();
        printf("halt\n");
        asm_halt();
    }
}
```

`program_exit`会将返回的线程的状态置为DEAD，然后调度下一个可执行的线程上处理器。注意，我们规定第一个线程是不可以返回的，这个线程的pid为0。

**第二种情况是之前被换下处理器的线程现在又被调度，其栈结构如下所示。**

![1715737995722](image/lab5/1715737995722.png)

执行4个 `pop`后，之前保存在线程栈中的内容会被恢复到这4个寄存器中，然后执行ret后会返回调用 `asm_switch_thread`的函数，也就是 `ProgramManager::schedule`，然后在 `ProgramManager::schedule`中恢复中断状态，返回到时钟中断处理函数，最后从时钟中断中返回，恢复到线程被中断的地方继续执行。

这样，通过 `asm_switch_thread`中的 `ret`指令和 `esp`的变化，我们便实现了线程的调度。

### 创新改进：不同的调度算法实现

**对教程中得某些部分改进：（以RR为例）**

1、对 `kernel/interrupt.cpp`中断程序改变使其可以输出进程具体得运行时间：

```
extern "C" void c_time_interrupt_handler()
{
    PCB *cur = programManager.running;

    if (allticks&&cur->ticks)
    {
        --cur->ticks;
        ++cur->ticksPassedBy;
        --allticks;
        printf("pid %d name \"%s\" have run %d\n",cur->pid,cur->name,cur->ticksPassedBy);
    }
    else
    {
    	if (threadnum>0&&cur->ticks==0) {
    		printf("---pid %d name \"%s\" is successfully done\n", programManager.running->pid, programManager.running->name);
    		threadnum--;
    		if (threadnum==0) printf("----------All threads have done");
    	}
    	if (threadnum>0) printf("----------rotation time slice\n");
    	allticks=2;
        programManager.schedule();
    }
}
```

2、对 `kernel/program.cpp`中的 `ProgramManager::schedule`和 `programManager::executeThread`和 `ProgramManager::exit`改进：

```
void ProgramManager::schedule()
{
    bool status = interruptManager.getInterruptStatus();
    interruptManager.disableInterrupt();  //关闭中断
    if (readyPrograms.size() == 0)
    {
        interruptManager.setInterruptStatus(status);
        return;
    }

  
    if (running->ticks==0) running->status=ProgramStatus::DEAD;
  
    if (running->status == ProgramStatus::RUNNING)
    {
        running->status = ProgramStatus::READY;
        //running->ticks = running->priority * 4;
        readyPrograms.push_back(&(running->tagInGeneralList));
    }
    else if (running->status == ProgramStatus::DEAD)
    {
        releasePCB(running);
    }

    ListItem *item = readyPrograms.front();
    PCB *next = ListItem2PCB(item, tagInGeneralList);
    PCB *cur = running;
  
    next->status = ProgramStatus::RUNNING;
    running = next;
    readyPrograms.pop_front();
  
  

    //printf("!!!!%d %d %d\n",next->pid,next->ticks,next->status);
    asm_switch_thread(cur, next);  //进程切换

    interruptManager.setInterruptStatus(status);  //恢复中断
}
```

```

```

```
void program_exit()
{
    PCB *thread = programManager.running;
    //thread->status = ProgramStatus::DEAD;
    if (thread->ticks==0) thread->status = ProgramStatus::DEAD;
  
    if (thread->pid)
    {
    	//while (1);
        //programManager.schedule();
        asm_halt();  //回到中断
    }
    else
    {
    	while (1);
        interruptManager.disableInterrupt();
        printf("halt\n");
        asm_halt();
    }
}
```

运用不同的调度算法实现（可以用到数据结构去控制，目前的理解是进程就是一个文件（有各种状态））

对 `list.h`的理解

要用到中断

~~Q2创新点：用不同的调度算法是实现（4种基本的）~~

重复assignment3，用不同调度算法实现切换

首先，给不同的进程以不同的执行时间和优先级：

在 `src/kernel/program.cpp`中的 `programManager::executeThread`函数中对时间重新赋值：

```
if (thread->pid==0) thread->ticks=1;
if (thread->pid==1) thread->ticks=5;
if (thread->pid==2) thread->ticks=3;
if (thread->pid==3) thread->ticks=2;
if (thread->pid==4) thread->ticks=1;
```

然后我们将helloworld的输出也放在这里：

这里用helloworld表示进程创建

```
printf("pid %d name \"%s\": Hello World! thread time:%d priority:%d\n", thread->pid, thread->name,thread->ticks,thread->priority);
```

#### 1、先来先服务调度FCFS：

代码见 `src/task3/FCFS/src/kernel`文件夹里面得.cpp文件

按照进程到达CPU时间加入就绪队列。

```
readyPrograms.push_back(&(thread->tagInGeneralList));
```

#### 2、优先级调度：

按照优先级顺序加入就绪队列

改变 `kernel/program.cpp`核心代码如下：

```
ListItem *item = readyPrograms.front();
    PCB *x = ListItem2PCB(item, tagInGeneralList);
    int flag=1;
    for (int i=1;i<=readyPrograms.size();++i)	{
    	if (x->priority > thread->priority) {  	  
    	    flag=0;
    	    readyPrograms.insert(i-1, &(thread->tagInGeneralList));
	    break;
    	}
    	item=item->next;
    	x = ListItem2PCB(item, tagInGeneralList);
    }
    if (flag) readyPrograms.push_back(&(thread->tagInGeneralList));
```

#### 3、短作业优先调度SJF：

在加入就绪队列中按执行时间排序：

```
//按照作业时间排序加入readyPrograms
   
    ListItem *item = readyPrograms.front();
    PCB *x = ListItem2PCB(item, tagInGeneralList);   
    int flag=1;
    for (int i=1;i<=readyPrograms.size();++i)	{
    	//printf(",");
    	if (x->ticks > thread->ticks) {
    	    flag=0;
    	    readyPrograms.insert(i-1, &(thread->tagInGeneralList));
	    break;
    	}
    	item=item->next;
    	x = ListItem2PCB(item, tagInGeneralList);
    }
    if (flag) readyPrograms.push_back(&(thread->tagInGeneralList));
```

#### 4、最高响应比优先调度（HRRN）：

$R=(W+S)/S$ **(**W**为等待时间、**S**为预计执行时间**)

到达时间假如定义为2.0*进程的pid（如pid 1->0.2）

则等待时间为wait（wait是前面的等待时间）+2.0*（5-pid）（达到时间不同带来的等待时间）

每次从就绪队列中选取响应比最大：

```
ListItem *item = readyPrograms.front();
    PCB *p = ListItem2PCB(item, tagInGeneralList);
    PCB *cur = running;
  
    PCB *next=p;
    int x=1;
    double s=(wait+2.0*(5-p->pid))/p->ticks;
   
    for (int i=2;i<=readyPrograms.size();++i)	{
    	item=item->next;
    	p=ListItem2PCB(item, tagInGeneralList);
     	double sp=(wait+2.0*(5-p->pid))/p->ticks;
     	if (sp>s) {
     	    s=sp;
     	    x=i;
     	    next=p;
     	}
    }
    //readyPrograms.pop_front();
    wait=wait+p->ticks;
    //printf("!!1%d\n",wait);
    readyPrograms.erase(x-1);
    next->status = ProgramStatus::RUNNING;
    running = next;
```

#### 5、轮转调度算法RR：

设计一个时间片，每次进程进行完或者时间片用完就进行轮转

代码如下：

`program::schedule`函数

```
    if (running->ticks==0) running->status=ProgramStatus::DEAD;
  
    if (running->status == ProgramStatus::RUNNING)
    {
        running->status = ProgramStatus::READY;
        //running->ticks = running->priority * 4;
        readyPrograms.push_back(&(running->tagInGeneralList));
    }
    else if (running->status == ProgramStatus::DEAD)
    {
        releasePCB(running);
    }

    ListItem *item = readyPrograms.front();
    PCB *next = ListItem2PCB(item, tagInGeneralList);
    PCB *cur = running;
  
    next->status = ProgramStatus::RUNNING;
    running = next;
    readyPrograms.pop_front();
```

`interrupt.cpp`中的函数

```
extern "C" void c_time_interrupt_handler()
{
    PCB *cur = programManager.running;

    if (allticks&&cur->ticks)
    {
        --cur->ticks;
        ++cur->ticksPassedBy;
        --allticks;
        printf("pid %d name \"%s\" have run %d\n",cur->pid,cur->name,cur->ticksPassedBy);
    }
    else
    {
    	if (threadnum>0&&cur->ticks==0) {
    		printf("---pid %d name \"%s\" is successfully done\n", programManager.running->pid, programManager.running->name);
    		threadnum--;
    		if (threadnum==0) printf("----------All threads have done");
    	}
    	if (threadnum>0) printf("----------rotation time slice\n");
    	allticks=2;
        programManager.schedule();
    }
}
```

# 实验过程

## assignment1：

输出printf的功能：

![1715830541066](image/lab5/1715830541066.png)

## assignment2：

创建第一个进程：

![1715830622492](image/lab5/1715830622492.png)

## assignment3：

### 说明:

#### 1、运行程序

```
终端进入到src/build
make
make run 
```

#### 2、debug查看

```
终端进入到src/build
make debug
layout src
b 4  ;设置断点
c
s    ;单步进入
s    ;单步进入
```

进入到 `interrupt.cpp`程序中，然后设置想要的断点，单步运行即可

展示：（以RR程序为例）

![1715830725004](image/lab5/1715830725004.png)

![1715830752055](image/lab5/1715830752055.png)

![1715830775129](image/lab5/1715830775129.png)

![1715830829658](image/lab5/1715830829658.png)

![1715830984009](image/lab5/1715830984009.png)

#### 3、运行数据：

| pid      | pid 0        | pid 1         | pid 2        | pid 3        | pid 4        |
| -------- | ------------ | ------------- | ------------ | ------------ | ------------ |
| 线程     | first thread | second thread | third thread | forth thread | fifth thread |
| 优先级   | 1            | 5             | 4            | 2            | 3            |
| 执行时间 | 1            | 5             | 3            | 2            | 1            |

### 五种调度算法实现：

注意，没有NULL的定义

SJF和PS（优先级调度）：采用在假如readyProgram时就已经是拍好序了

HRRN是在取出来的时候排序

SJF中有个问题：由于第0个进程师父进程是不可返回的，所以开始的时候父进程就在running队列中了！

#### 1、FSFC：

![1715831258648](image/lab5/1715831258648.png)

#### 2、PS：

![1715831334632](image/lab5/1715831334632.png)

#### 3、SJF：

![1715831378472](image/lab5/1715831378472.png)

#### 4、HRRN：

![1715831472623](image/lab5/1715831472623.png)

#### 5、RR：

![1715830956772](image/lab5/1715830956772.png)

![1715831015613](image/lab5/1715831015613.png)

# 实验总结：

本次实验可以说是这几个实验中写的东西最多的实验报告了，也可以说是最详细的实验报告。

在本次实验中，我更正了许多教程中没有讲到的东西，同时也学习到了不同调度算法对于进程的应用。

同时我更加理解了在单CPU下的防止互斥RC现象的方法：中断（关中断和恢复中断）

同时本次实验学会了用git（因为文件实在是太多了），还是非常的方便

参考借鉴的网站：

```
https://gitee.com/all-about-git
```

![1715828820863](image/lab5/1715828820863.png)

![1715829320015](image/lab5/1715829320015.png)

要用下面的命令，然后输入你的名字和密码：

```
git add .
git push
```

![1715830389328](image/lab5/1715830389328.png)

## 实验中遇到的问题和解决：

问题1：教程中的schedule的调度算法有问题——子进程进入调度序列中但是不会进入中断

解决办法：在 `programManager.schedule();`前面

或者——写成 `asm_halt();`也就是返回中断

```
void program_exit()
{
    PCB *thread = programManager.running;
    thread->status = ThreadStatus::DEAD;

    if (thread->pid)
    {
	while(1);
        programManager.schedule();
    }
    else
    {
        interruptManager.disableInterrupt();
        printf("halt\n");
        asm_halt();
    }
}
```

```
void program_exit()
{
    PCB *thread = programManager.running;
    thread->status = ThreadStatus::DEAD;

    if (thread->pid)
    {
        asm_halt();
    }
    else
    {
        interruptManager.disableInterrupt();
        printf("halt\n");
        asm_halt();
    }
}
```

![1715741347653](image/lab5/1715741347653.png)

问题2：为什么每次重新回到进程1都会结束？原因——子进程的状态是dead！！

还是在program_exit()函数里面的问题——直接把thread->dead

![1715743689552](image/lab5/1715743689552.png)

问题三：如何让它先生成完所有的进程，再运行调度算法？

![1715757733226](image/lab5/1715757733226.png)

其实都是已经生成完了，但是function的调用导致它要运行一次才能输出一次function。

问题4：为什么顺序会乱？就是插入一个printf("")之后，难道中断时间问题？删掉就没问题了

原因：make比较慢，还没有完成，这时候make run就会有这种情况，如果等一会就不会了。

![1715775504791](image/lab5/1715775504791.png)

![1715775546329](image/lab5/1715775546329.png)

![1715821416710](image/lab5/1715821416710.png)

问题5：

在PS优先级调度中，遇到了异常中断的现象（4个进程的时候还是OK的，5个进程就异常了？）

原因：是list插入写错了，可以直接用readyPrograms.insert（）这个函数（注意是i-1,不是i）

![1715828785631](image/lab5/1715828785631.png)

# 参考文献

（1）https://gitee.com/guifeng/sysu-2021-spring-operating-system

（2）https://www.cnblogs.com/clover-toeic/p/3736748.html——[可变参数函数详解 - clover_toeic - 博客园 (cnblogs.com)](https://www.cnblogs.com/clover-toeic/p/3736748.html)

（3）https://zhuanlan.zhihu.com/p/97071815——[详解操作系统内核对线程的调度算法 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/97071815)

（4）[常用的调度算法（包含实例）|操作系统-CSDN博客](https://blog.csdn.net/weixin_46308081/article/details/120750507)

（5）[最高响应比优先算法（HRRF）及例题详解 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/650242750)
